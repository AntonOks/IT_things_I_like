# Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

## Great Examples

### WebFreaks DUB Doku

* [GitHub repository](https://github.com/WebFreak001/dub-docs-v2)
  * [Web Site](https://docs.webfreak.org/)

### chezmoi - Manage your dotfiles across multiple diverse machines, securely.

* [GitHub repository](https://github.com/twpayne/chezmoi/tree/master/assets/chezmoi.io)
  * [Web Site](https://www.chezmoi.io/)
