# [RCLONE home page](https://rclone.org/)

## Configure GMX Cloud

See either ```rclone config show``` or "C:\Users\oks\scoop\apps\rclone\current\rclone.conf":

    rclone config show
    [GMX_Cloud]
    type = webdav
    url = https://webdav.mc.gmx.net
    vendor = other
    user = anton.oks@gmx.de
    pass = wwJL3P2zvaW1frCsfm7zeymwsBGRN4jWkEOfdh9ynQ

### Download a list of my GMX Cloud books

    rclone.exe lsf "GMX_Cloud:Meine Dokumente/Buecher/" -R > GMX_Cloud_Buecher.txt

### Upload a hole local directory to GMX

    rclone moveto "C:\Users\anton\Downloads\Packt Free Learning Source" 'GMX_Cloud:Meine Dokumente/Buecher/Packt Free Learning Source' --dry-run
    rclone moveto "C:\Users\anton\Downloads\Packt Free Learning Source" 'GMX_Cloud:Meine Dokumente/Buecher/Packt Free Learning Source' -P

### Mount a RCLONE configured repo locally on my Windows system

    mkdir C:\Temp\RCLONE
    rclone mount GMX_Cloud: C:\Temp\RCLONE\GMX_Cloud --vfs-cache-mode full --vfs-cache-max-size 1G -vv
