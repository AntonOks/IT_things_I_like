#!/usr/bin/bash

# Script to mantain log in mroot to avoid the overwriting
#
# Tunable parameters
# FOLDER    Where the logs are
# LOG       Base filename for the logs to keep
# RETAIN    Number of files to keep
#

FOLDER="/mroot/etc/log/mlog"
LOG="secd.log.0"
RETAIN=1000

cd $FOLDER

# Keeping all the files existing
for foo in `ls $LOG* `
do
    /bin/ln $foo keep-$foo >& /dev/null
done

ACTUAL=`ls keep-$LOG* | wc -l`
# Checking the number of files
if [ $ACTUAL -gt $RETAIN ]
then
    REMOVE=`expr $ACTUAL - $RETAIN`
    ls keep-$LOG* | head -n $REMOVE | xargs /bin/rm 
fi

