# Great Examples

- "checkver": "script"
    - [refreshenv](https://github.com/ScoopInstaller/Main/blob/master/bucket/refreshenv.json#L10)

      ```json
      "checkver": {
           "script": [
               "# Using script to get version number from date, e.g. 6 Mar, 2019 -> 2019.03.06",
               "$url = 'https://github.com/chocolatey/choco/commits/HEAD/src/chocolatey.resources/redirects/RefreshEnv.cmd'",
               "$regex = 'Commits on ([\\w\\s,]+)</h2>'",
               "$cont = $(Invoke-WebRequest $url).Content",
               "if(!($cont -match $regex)) { error \"Could match '$regex' on '$url'\"; return }",
               "$script_ver = $(Get-Date $matches[1]).ToString('yyyy.MM.dd')",
               "Write-Output $script_ver"
           ],
           "regex": "([\\d.]+)"
          }
      ```

# Open the "home" of all my manifests

```powershell
Get-ChildItem -Path .\bucket\ -Recurse -Filter *.json | ForEach-Object { $Manifest = [System.IO.Path]::GetFileNameWithoutExtension($_.Name); &scoop home $Manifest }
```
