# IT_things_I_like

Just everything about IT, programming, tools ... I like and/or use

## Mkdocs quick quide

### Work on docs locally

You might want to create a virtualenv if you work with python or other mkdocs sites a lot

```python
python3 -m pip install -r requirements.txt
mkdocs serve
```

### Build static pages to distribute

```python
mkdocs build
```
